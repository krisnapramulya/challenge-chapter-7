import axios from 'axios';
import { useEffect, useState } from "react";
import { Navigate } from 'react-router-dom';
import Footer from '../components/Footer'
import Header2 from '../components/Header2'
import { addUser } from '../slices/userSlice';
import { useDispatch } from "react-redux";
import CarList from '../components/Car';
import { setFilter } from "../reducers/api-store";
import { Button, Card, Nav, Navbar, } from 'react-bootstrap'
// import { BsCalendar3, BsGearFill, BsPeopleFill } from 'react-icons/bs';
import '../components/css/cars.css'


const Cars = () => {
    const dispatch = useDispatch();
    const [isLoggedIn, setIsLoggedIn] = useState(true);
    const [user, setUser] = useState({});
    const [driver, setDriver] = useState();
    const [date, setDate] = useState();
    const [time, setTime] = useState();
    const [passengger, setPassengger] = useState();

    const [data, setData] = useState({});

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserRequest = await axios.get(
                    "http://localhost:2000/auth/me",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                );

                const currentUserResponse = currentUserRequest.data;

                if (currentUserResponse.status) {
                    dispatch(
                        addUser({
                            user: currentUserResponse.data.user,
                            token: token,
                        })
                    )
                    setUser(currentUserResponse.data.user);
                }
            } catch (err) {
                setIsLoggedIn(false);
            }
        };

        fetchData();
        dispatch(setFilter(data));
    }, [dispatch, data]);

    const logout = () => {
        localStorage.removeItem("token");

        setIsLoggedIn(false);
        setUser({});
    };


    const showBackdrop = () => {
        document.querySelector(".modal-backdrop").style.display = "block"
        document.body.classList.add("modal-open")
    }

    const hideBackdrop = (e) => {
        document.querySelector(".modal-backdrop").style.display = "none"
        document.body.classList.remove("modal-open")
        e.target.blur()
    }
    const onSearch = (e) => {
        e.preventDefault();
        setData({
            driver: driver,
            date: date,
            time: time,
            passengger: parseInt(passengger),
        });
    };

    return isLoggedIn ? (
        <>
            {/* navbar */}
            <nav className="navbar navbar-expand-lg navbar-light bg-all">
                <div className="container">
                    <Navbar.Brand href='/' className='brand' />
                    <div className="offcanvas-body" id="offcanvasRight">
                        <Card border='primary' className='h-50 mt-2'>hello kak! {user.name}</Card>
                        <div className="navbar-nav ms-auto">
                            <Nav.Link className='text-dark pe-3'>Our Services</Nav.Link>
                            <Nav.Link className='text-dark pe-3'>Why Us</Nav.Link>
                            <Nav.Link className='text-dark pe-3'>Testimonial</Nav.Link>
                            <Nav.Link className='text-dark pe-3'>FAQ</Nav.Link>
                            <Button variant="danger" type="button" className="btn" onClick={(e) => logout(e)}>Logout</Button>
                        </div>
                    </div>
                </div>
            </nav>
            <Header2 />
            <div className="container mt-5">
                <div id="filter-box">
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-10 col-md-12">
                                    <form className="needs-validation" noValidate>
                                        <div className="row">
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group1"><label>Tipe Driver</label>
                                                    <select
                                                        id="select-driver"
                                                        className="form-control"
                                                        onFocus={showBackdrop}
                                                        onBlur={hideBackdrop}
                                                        onChange={(e) => setDriver(e.target.value)}
                                                    >
                                                        <option>Pilih Tipe Driver</option>
                                                        <option value="1">Dengan Sopir</option>
                                                        <option value="2">Tanpa Sopir</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group2">
                                                    <label>Tanggal</label>
                                                    <input
                                                        id="date"
                                                        type="date"
                                                        className="form-control"
                                                        onFocus={showBackdrop}
                                                        onBlur={hideBackdrop}
                                                        onChange={(e) => setDate(e.target.value)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group3">
                                                    <label>Waktu Jemput/Ambil</label>
                                                    <input
                                                        id="waktu"
                                                        type="time"
                                                        className="form-control"
                                                        onFocus={showBackdrop}
                                                        onBlur={hideBackdrop}
                                                        onChange={(e) => setTime(e.target.value)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group4">
                                                    <label>Jumlah passengger</label>
                                                    <div className="input-group">
                                                        <input
                                                            id="passengger"
                                                            type="number"
                                                            className="form-control border-end-0"
                                                            placeholder="Jumlah passengger"
                                                            onFocus={showBackdrop}
                                                            onBlur={hideBackdrop}
                                                            onChange={(e) => setPassengger(e.target.value)}
                                                        />
                                                        <div className="input-group-append"></div>
                                                        <div className="input-group-text bg-white">
                                                            <img src="/images/fi_users.png" alt='' style={{ width: 24 }} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-2 col-md-12">
                                    <div className="form-group" id="form-group5"><label>&nbsp;</label>
                                        <button
                                            id="button-cari"
                                            type="button"
                                            className="btn btn-success col-sm-12"
                                            onFocus={showBackdrop}
                                            onBlur={hideBackdrop}
                                            onClick={onSearch}

                                        >
                                            Cari Mobil
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <CarList />
                <Footer />
            </div>
        </>
    ) : (
        <Navigate to="/login" replace />
    );
}

export default Cars