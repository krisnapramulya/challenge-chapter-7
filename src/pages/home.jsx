import Navbar from '../components/Navbar'
import Header from '../components/Header'
import Ourservice from '../components/Ourservice'
import Whyus from '../components/Whyus'
import Testimoni from '../components/Testimoni'
import Banner from '../components/Banner'
import Faq from '../components/Faq'
import Footer from '../components/Footer'
import '../components/css/style.css'

const Home = () => {
    return (
        <>
           <Navbar />
            <Header />
            <Ourservice />
            <Whyus />
            <Testimoni />
            <Banner />
            <Faq />
            <Footer />
        </>
    )
}

export default Home